-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextNormalizeSlashes"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Unify the slash direction on filepaths.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    InText = self:AddInput("Text", "Text", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })

    InSlashDirection = self:AddInput("Slash Direction", "SlashDirection", {
        LINKID_DataType = "Number",
        INPID_InputControl = "MultiButtonControl",
        MBTNC_ForceButtons = true,
        --MBTNC_ShowBasicButton = false,
        MBTNC_ShowName = true,
        INP_External = false,
        INP_Default = 0,
        {MBTNC_AddButton = "Windows" , MBTNCD_ButtonWidth = 0.5},
        {MBTNC_AddButton = "Linux" , MBTNCD_ButtonWidth = 0.5},
    })

    InRemoveDuplicates = self:AddInput("Remove Duplicate Slashes", "RemoveDuplicates", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function Process(req)
    -- [[ Creates the output. ]]
    local str = InText:GetValue(req).Value
    local direction = InSlashDirection:GetValue(req).Value
    local remove_dup = InRemoveDuplicates:GetValue(req).Value
    local out = ""

    out = NormalizeSlashes(str, direction)

    if remove_dup == 1.0 then
      out = RemoveDupSlashes(out)
    end

    OutText:Set(req, Text(out))
end

function RemoveDupSlashes(path)
    path = string.gsub(path, [[//]], [[/]])
    path = string.gsub(path, [[\\]], [[\]])

    return path
end

function NormalizeSlashes(path, mode)
    if mode == 0 then
        -- Windows
        path = string.gsub(path, [[/]], [[\]])
    else
        -- Linux
        path = string.gsub(path, [[\]], [[/]])
    end

    return path
end
