-- ============================================================================
-- modules
-- ============================================================================
local textutils = self and require("vtextutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextSubStripRight"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Substring",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Strips a trailing substring of a string.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Text", "Text", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })

    InStrip = self:AddInput("Strip", "Strip", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InText:SetAttrs({LINK_Visible = visible})
        InStrip:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local text = InText:GetValue(req).Value
    local strip = InStrip:GetValue(req).Value

    local stripped = textutils.rstrip(text, strip)
    local out = Text(stripped)

    OutText:Set(req, out)
end
