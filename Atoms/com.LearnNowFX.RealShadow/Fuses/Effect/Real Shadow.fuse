FuRegisterClass("RealShadow", CT_Tool, {
  REGS_Name = "Real Shadow",
  REGS_Category = "Learn Now FX",
  REGS_OpIconString = "LShd",
  REGS_OpDescription = "Realistic Soft Shadow Plug-in",
  REG_Version = 200,
  REGS_Company = "Learn Now FX",
  REGS_URL = "http://LearnNowFX.com",
  REGS_HelpTopic  = "https://www.facebook.com/groups/DavinciResolveFusion",
  REG_Fuse_NoEdit	= true,
  REG_Fuse_NoReload = true,
})
 
--------------------------------
-- Description of kernel parameters
 
params = [[
    float blend;
    float decay;
    float weight;
    float exposure;
    float threshold;
    float gamma;
    float low;
    float high;
    int mergeOver;
    float center[2];
    float color[4];
    int workSize[2];
    int compOrder;
]]
 
--------------------------------
-- source of kernel
 
source = [[
//A hige thanks to my friend Chris Ridings for helping me with this function!!
__DEVICE__ inline float powcf(float x, float y) {
	float ret = _powf(x,y);
	if (isnan(ret)) {
		ret = 0.0001f;
	}
	return ret;
}
__DEVICE__ float thresh(float low, float high, float input) {

//This threshold function was taught to me by Chad from the WSL forums. Thanks Chad!!
if (_fabs(high - low) < 0.000001f){ input *= 0.0f;}  
if (_fabs(high  - low) > 0.000001f){ input = (input - low) / (high  - low);} 

    return input;

}
__DEVICE__ inline void EvaluateShadow(__TEXTURE2D__ in, __TEXTURE2D__ src, __TEXTURE2D_WRITE__ out, int in_RayNum, int2 imgSize,
        float blend, float2 center,float Decay, float Weight, float InvExposure, float fThreshold, float gamma, float low, float high, int iMergeOver, float3 shadowColor,
        int compOrder)
{
    int LightScreenPosX = imgSize.x * center.x;
    int LightScreenPosY = imgSize.y * center.y;
 
    //------------------------------------------------------------
    //-----------God Rays OpenCL HDR Post Processing here---------
    //------------------------------------------------------------
    //in_pData params
    float _InvExposure = InvExposure;
 
    // Width of the image
    int x_last = imgSize.x - 1;
    // Height of the image
    int y_last = imgSize.y - 1;
    // X coordinate of the God Rays light source
    int x = LightScreenPosX;
    // Y coordinate of the God Rays light source
    int y = LightScreenPosY;
    // X coordinate of the God Rays light source
    int x_s = LightScreenPosX;
    // Y coordinate of the God Rays light source
    int y_s = LightScreenPosY;
 
    /* ***********************************************
        Calculate the destination point of the rays
        and check to see if the rays intersect the image
     *********************************************** */
 
    // Initialize the destinations to zero representing the
    // lower-left corner of the image, the (0,0) coordinate
    int x_dst = 0;
    int y_dst = 0;
    int x_dst_s = 0;
    int y_dst_s = 0;
 
    // Use the ray number as a destination pixel of the bottom border and check the range.
    int Ray = in_RayNum;
    if (Ray < x_last)
    {
        // In this case, check to see if the ray is located outside of the image or
        // the ray from the left bottom corner pixel (also directed from the left).
        if (y < 0 &&
            (x < 0 || Ray != 0))
        {
            // These rays do not paint any pixels, so you have no work to do
            // and you can exit the function.
            return;
        }
        // If you still have work to do, set up the destination points for the original
        // and shadow rays as a ray number.
        x_dst = Ray;
        y_dst = 0;
        x_dst_s = Ray - 1;
        y_dst_s = 0;
 
        // If the original ray is a corner ray, fix the shadow ray
        // destination point, because it places at another border.
        if (Ray == 0)
        {
            x_dst_s = 0;
            y_dst_s = 1;
        }
    }
 
     // If the ray number is greater than the bottom border range, check other borders.
    else
    {
        // Fix the ray number by the bottom border length to exclude those
        // pixels from computation.
        Ray -= x_last;
 
        // Set the ray number as a destination pixel of the right border and check the range.
        if (Ray < y_last)
        {
            // To see what you need to do, check if the ray is located out from
            // the image and the ray from the right bottom corner pixel is also
            // directed from the right.
            if (x > x_last &&
                (y < 0 || Ray != 0))
            {
                return;
            }
 
            // Set up destination points for original and shadow rays as the
            // ray number if there is some work to do.
            x_dst = x_last;
            y_dst = Ray;
            x_dst_s = x_last;
            y_dst_s = Ray - 1;
 
            // Fix the shadow ray destination point, in case the original ray is a corner ray.
            if (Ray == 0)
            {
                x_dst_s = x_last - 1;
                y_dst_s = 0;
            }
        }
 
        // Repeat the steps for the top border if the ray number doesn't
        // correspond to the right border.
        else
        {
            Ray -= y_last;
            if (Ray < x_last)
            {
                if (y > y_last &&
                    (x > x_last || Ray != 0))
                {
                    return;
                }
                x_dst = x_last - Ray;
                y_dst = y_last;
                x_dst_s = x_last - Ray + 1;
                y_dst_s = y_last;
                if (Ray == 0)
                {
                    x_dst_s = x_last;
                    y_dst_s = y_last - 1;
                }
            }
 
            // Check left borders if the ray number doesn't correspond to the top one.
            else
            {
                Ray -= x_last;
                if (Ray < y_last)
                {
                    if (x < 0 &&
                        (y > y_last || Ray != 0))
                    {
                        return;
                    }
                    x_dst = 0;
                    y_dst = y_last - Ray;
                    x_dst_s = 0;
                    y_dst_s = y_last - Ray + 1;
                    if (Ray == 0)
                    {
                        x_dst_s = 1;
                        y_dst_s = y_last;
                    }
                }
                else
                {
                    // If the ray number doesn't correspond to any border, you have no
                    // more work to do, so exit from the function.
                    return;
                }
            }
        }
    }
    // Calculate start and destination points sizes for each dimension
    // and select directions as the steps size.
    int dx = x_dst - x;
    int dy = y_dst - y;
    int dx_s = x_dst_s - x;
    int dy_s = y_dst_s - y;
    int xstep = dx > 0 ? 1 : 0;
    int ystep = dy > 0 ? 1 : 0;
    int xstep_s = dx_s > 0 ? 1 : 0;
    int ystep_s = dy_s > 0 ? 1 : 0;
    if (dx < 0)
    {
        dx = -dx;
        xstep = -1;
    }
    if (dy < 0)
    {
        dy = -dy;
        ystep = -1;
    }
    if (dx_s < 0)
    {
        dx_s = -dx_s;
        xstep_s = -1;
    }
    if (dy_s < 0)
    {
        dy_s = -dy_s;
        ystep_s = -1;
    }
    // Calculate Bresenham parameters for the original ray and correct the
    // Decay value by the step length.
    float FixedDecay = 0.0f;
    int di = 0;
    int di_s = 0;
    int steps = 0;
 
 
    // Select the biggest dimension of the original ray.
 
    // If dx is greater than or equal to dy, use dx as the steps count.
    if (dx >= dy)
    {
        // Fix Decay by the step length computed as a ray length divided by steps count.
        FixedDecay = _expf(Decay * _sqrtf((float)dx *
            (float)dx + (float)dy * (float)dy) / (float)dx);
 
        // Set up the Bresenham delta error.
        di = 2 * dy - dx;
 
        // Crop the ray by the image borders to reduce the steps cont.
 
        // If the horizontal ray (dx >= dy) crosses over a left or right
        // border, you should crop it by the step count. In other words, you
        // know the first step in image borders and need to find the
        // corresponding y coordinate and the delta error value.
        if (x < 0 || x > x_last)
        {
            // Select the count of steps that need to be omitted.
            int dx_crop = x < 0 ? -x : x - x_last;
 
            // Fix the delta error value by the omitted steps on X dimension.
            int new_di = di + (dx_crop - 1) * 2 * dy;
 
            // Calculate the appropriate steps count for Y dimension.
            int dy_crop = new_di / (2 * dx);
 
            // Check the final delta error value.
            new_di -= dy_crop * 2 * dx;
 
            // If a new delta error value is greater than 0, move by the Y
            // dimension when you cross the image range.
            if (new_di > 0)
            {
                // In that case, increase the steps count on Y dimension by 1.
                dy_crop++;
            }
 
            // Recalculate the current position and the delta error value.
            x += xstep * dx_crop;
            y += ystep * dy_crop;
            di += dx_crop * 2 * dy - dy_crop * 2 * dx;
        }
 
        // If the horizontal ray (dx >= dy) crosses either the bottom or top
        // border, you should crop it by the y coordinate. You know the y coordinate
        // and need to find the first corresponding step in the image borders and
        // the delta error value. */
        if (y < 0 || y > y_last)
        {
            // Select the count of steps that you should omit on the Y coordinate.
            int dy_crop = y < 0 ? -y : y - y_last;
 
            // Fix the delta error value by omitting steps on the Y dimension.
            int new_di = di - (dy_crop - 1) * 2 * dx;
 
            // Calculate the appropriate steps count for the X dimension.
            int dx_crop = 1 - new_di / (2 * dy);
 
            // If a new delta error value is less than 0 and this value can
            // be divided by 2dy without a remainder, move by the Y dimension
            // when you cross the image range.
            if (new_di % (2 * dy) != 0 && new_di < 0)
            {
                // Increase steps count on the X dimension by 1.
                dx_crop++;
            }
 
            // Since you know the right count of omitted steps in all
            // dimensions, you can recalculate the current position and the
            // delta error value.
            x += xstep * dx_crop;
            y += ystep * dy_crop;
            di += dx_crop * 2 * dy - dy_crop * 2 * dx;
        }
 
        // At the end of cropping the horizontal ray, calculate the correct steps count.
        steps = abs(x_dst - x);
    }
 
    // If dy is greater than dx, use dy as the count of steps and perform
    // exactly the same steps but with different coordinates.
    else
    {
 
        // Fix Decay by the step length, which is computed as the ray length divided by the steps count.
        FixedDecay = _expf(Decay * _sqrtf((float)dx *
            (float)dx + (float)dy * (float)dy) / (float)dy);
 
        // Set up the Bresenham delta error.
        di = 2 * dx - dy;
 
        // Crop the ray by the image borders to reduce the steps cont.
 
        // If the vertical ray (dy > dx) crosses either a bottom or top
        // border, crop it by the step count.
        if (y < 0 || y > y_last)
        {
            // Select the count of steps that should be omitted.
            int dy_crop = y < 0 ? -y : y - y_last;
 
            // Use the omitted steps to fix the delta error value on the y dimension.
            int new_di = di + (dy_crop - 1) * 2 * dx;
 
            // Calculate the appropriate steps count for the X dimension.
            int dx_crop = new_di / (2 * dy);
 
            // Check the final delta error value.
            new_di -= dx_crop * 2 * dy;
 
            // If a new delta error value is greater than zero,
            if (new_di > 0)
            {
                // increase the steps count on the X dimension by 1.
                dx_crop++;
            }
 
            // Recalculate the current position and the delta error value.
            x += xstep * dx_crop;
            y += ystep * dy_crop;
            di += dy_crop * 2 * dx - dx_crop * 2 * dy;
        }
 
        // If the vertical ray (dy > dx) crosses either a left or right
        // border, crop it on the X coordinate.
        if (x < 0 || x > x_last)
        {
            // Select the count of steps that should be omitted on the X  coordinate.
            int dx_crop = x < 0 ? -x : x - x_last;
 
            // Use omitted steps to fix the delta error value on the X dimension.
            int new_di = di - (dx_crop - 1) * 2 * dy;
 
            // Calculate the appropriate steps count for the Y dimension.
            int dy_crop = 1 - new_di / (2 * dx);
 
            // If a new delta error value is less than 0 and this value can
            // be divided by 2dy without a remainder, then ...
            if (new_di % (2 * dx) != 0 && new_di < 0)
            {
                // increase the steps count on X dimension by 1.
                dy_crop++;
            }
 
            // Recalculate the current position and the delta error value.
            x += xstep * dx_crop;
            y += ystep * dy_crop;
            di += dy_crop * 2 * dx - dx_crop * 2 * dy;
        }
 
        // After cropping the vertical ray, calculate the correct steps count.
        steps = abs(y_dst - y);
    }
 
    // Omit some steps either at the beginning or the end of the shadow ray to
    // make the steps by pixels correspond to the original ray.
    int steps_begin = 0;
    int steps_lsat = 0;
 
    // Crop the shadow ray by exactly the same computation as the original one.
    if (dx_s >= dy_s)
    {
        di_s = 2 * dy_s - dx_s;
        if (x_s < 0 || x_s > x_last)
        {
            int dx_crop_s = x_s < 0 ? -x_s : x_s - x_last;
            int new_di_s = di_s + (dx_crop_s - 1) * 2 * dy_s;
            int dy_crop_s = new_di_s / (2 * dx_s);
            new_di_s -= dy_crop_s * 2 * dx_s;
            if (new_di_s > 0)
            {
                dy_crop_s++;
            }
            x_s += xstep_s * dx_crop_s;
            y_s += ystep_s * dy_crop_s;
            di_s += dx_crop_s * 2 * dy_s - dy_crop_s * 2 * dx_s;
        }
        if (y_s < 0 || y_s > y_last)
        {
            int dy_crop_s = y_s < 0 ? -y_s : y_s - y_last;
            int new_di_s = di_s - (dy_crop_s - 1) * 2 * dx_s;
            int dx_crop_s = 1 - new_di_s / (2 * dy_s);
            if (new_di_s % (2 * dy_s) != 0 && new_di_s < 0)
            {
                dx_crop_s++;
            }
            x_s += xstep_s * dx_crop_s;
            y_s += ystep_s * dy_crop_s;
            di_s += dx_crop_s * 2 * dy_s - dy_crop_s * 2 * dx_s;
        }
 
        // For the horizontal shadow ray, calculate the omitted steps as the
        // difference between the current position, (it can be different
        // after cropping), and the original position.
        steps_begin = xstep_s * (x - x_s);
 
        // If steps at the shadow ray should be omitted (the count of
        // omitted steps is greater than 0), omit them.
        if (steps_begin > 0)
        {
            // Calculate a new delta error value and move the start X coordinate
            // to the same position that the original ray has.
            di_s += 2 * steps_begin * dy_s;
            x_s = x;
 
            // To move along the Y axes, do the following steps:
            if (di_s > 2 * dy_s)
            {
                // Correct the delta error value and ...
                di_s -= 2 * dx_s;
 
                // Move the current position by the Y dimension.
                y_s += ystep_s;
 
                // Move along the Y axes no more than one step. The distance
                // between rays must be no greater than 1; otherwise, it is
                // impossible to define the rays.
            }
        }
 
        // Define the count of steps that should be omitted at the end of
        // the shadow ray as the count of steps that should be compared,
        // (the count of shadow ray steps without steps omitted at the
        // beginning).
        steps_lsat = abs(x_dst_s - x_s) - steps_begin;
    }
    else // Vertical shadow rays
    {
        di_s = 2 * dx_s - dy_s;
        if (y_s < 0 || y_s > y_last)
        {
            int dy_crop_s = y_s < 0 ? -y_s : y_s - y_last;
            int new_di_s = di_s + (dy_crop_s - 1) * 2 * dx_s;
            int dx_crop_s = new_di_s / (2 * dy_s);
            new_di_s -= dx_crop_s * 2 * dy_s;
            if (new_di_s > 0)
            {
                dx_crop_s++;
            }
            x_s += xstep_s * dx_crop_s;
            y_s += ystep_s * dy_crop_s;
            di_s += dy_crop_s * 2 * dx_s - dx_crop_s * 2 * dy_s;
        }
        if (x_s < 0 || x_s > x_last)
        {
            int dx_crop_s = x_s < 0 ? -x_s : x_s - x_last;
            int new_di_s = di_s - (dx_crop_s - 1) * 2 * dy_s;
            int dy_crop_s = 1 - new_di_s / (2 * dx_s);
            if (new_di_s % (2 * dx_s) != 0 && new_di_s < 0)
            {
                dy_crop_s++;
            }
            x_s += xstep_s * dx_crop_s;
            y_s += ystep_s * dy_crop_s;
            di_s += dy_crop_s * 2 * dx_s - dx_crop_s * 2 * dy_s;
        }
 
        // For the vertical shadow ray, calculate the omitted steps by the
        // same computations but with different dimensions:
 
        // Setup the omitted steps at the beginning as the difference
        // between the current position and the original position.
        steps_begin = ystep_s * (y - y_s);
 
        // Omit the first steps at the shadow ray if required by checking
        // to see if the variable steps_begin is greater than zero.
        if (steps_begin > 0)
        {
            // Calculate a new delta error value
            di_s += 2 * steps_begin * dx_s;
 
            // Move the start X coordinate,
            y_s = y;
            if (di_s > 2 * dx_s)
            {
                // correct the delta error value, and move the current
                // position by the Y dimension.
                di_s -= 2 * dy_s;
                x_s += ystep_s;
            }
        }
 
        // Fix the count of omitted steps at the end.
        steps_lsat = abs(y_dst_s - y_s) - steps_begin;
    }
 
    // Load method parameters.
 
    float4 Decay_128 = to_float4_s(FixedDecay);
    FixedDecay = 1.0f - FixedDecay;
    float4 nDecay_128 = to_float4_s(FixedDecay);
    float4 NExposure_128 = to_float4_s(_InvExposure);
    float4 summ_128 = to_float4_s(0.0f);
 
    //Load the first pixel of the ray.
 
    float4 sample_128 = _tex2DVecN(in, x, y, compOrder);
 
    // Apply the exposure to it to scale color values to appropriate ranges.
    sample_128 = sample_128 * NExposure_128;
 
 
    // Check to see if this pixel can be used in a sum.
    if (sample_128.x > fThreshold || sample_128.y > fThreshold || sample_128.z > fThreshold)
    {
        // Update the sum if required.
        summ_128 = sample_128 * nDecay_128;
    }
 
    // Check to see if the result can be written to the output pixel (the pixel is not
    // shaded by the shadow ray and it is not the first ray that should
    // paint the sun position).
    if (x != x_s || y != y_s || (x_dst == 0 && y_dst == 0 && x == LightScreenPosX && y == LightScreenPosY))
    {
        // Add the current sum value corrected by the Weight parameter to
       // the output buffer if possible.
        float4 answer_128 = summ_128 * to_float4_s(Weight);
 
        answer_128 = answer_128 + sample_128;
        //Writing to output pixel
        _tex2DVec4Write(out, x, y, make_intensity(answer_128, compOrder));
    }
 
    // In the main loop, go along the original ray.
    for (int i = 0; i < steps; i++)
    {
        // Make steps in the Bresenham loop for the original ray.
        if (dx >= dy)
        {
            x += xstep;
            if (di >= 0)
            {
                y += ystep;
                di -= 2 * dx;
            }
            di += 2 * dy;
        }
        else
        {
            y += ystep;
            if (di >= 0)
            {
                x += xstep;
                di -= 2 * dy;
            }
            di += 2 * dx;
        }
 
        // Make steps for the shadow rays if if they should not be omitted.
        if (steps_begin >= 0)
        {
            if (dx_s >= dy_s)
            {
                x_s += xstep_s;
                if (di_s >= 0)
                {
                    y_s += ystep_s;
                    di_s -= 2 * dx_s;
                }
                di_s += 2 * dy_s;
            }
            else
            {
                y_s += ystep_s;
                if (di_s >= 0)
                {
                    x_s += xstep_s;
                    di_s -= 2 * dy_s;
                }
                di_s += 2 * dx_s;
            }
        }
        else
        {
            steps_begin++;
        }
         // For each step, load the next pixel of the ray.
        float4 source_128 = _tex2DVecN(in, x, y, compOrder);
        sample_128 = source_128;
 
        // Apply the exposure to it to scale color values to appropriate ranges.
        summ_128 =  summ_128 * Decay_128;
 
        // Update the sum
        sample_128 = sample_128 * NExposure_128;
        // check if this pixel can be used in a sum and
        if (sample_128.x > fThreshold || sample_128.y > fThreshold || sample_128.z > fThreshold)
        {
            sample_128 = sample_128 * nDecay_128;
            summ_128 = summ_128 + sample_128;
        }
 
 
        // Check if it is possible to write the result to the output pixel.
        if (x != x_s || y != y_s || i >= steps_lsat)
        {
            // Update the output buffer by the current sum value
            // corrected by the Weight parameter.
            float4 answer_128 = summ_128 * to_float4_s(Weight);
 
            // Hide Source
           
           
         float4 shColor = to_float4_aw(shadowColor, 1.0f);
         float preShadow = make_intensity(answer_128, compOrder).x;
         preShadow = powcf(preShadow, 1.0f/gamma);
         preShadow = thresh(low, high, preShadow);
         preShadow = _clampf(preShadow, 0.0f, 1.0f);
         float4 lightShadow = to_float4_s(preShadow);
		 float4 originalImage = _tex2DVecN(src, x, y, compOrder);
		 
         float4 shadow = shColor * lightShadow;
         float4 shadowed;
         
            if (iMergeOver)
            {
                shadowed = _mix(shadow,  originalImage, originalImage.w);
            }
            else
            {
                shadowed = _mix(to_float4_s(0.0f), shadow, to_float4_s(blend));
            }
 
            _tex2DVec4Write(out, x, y, shadowed);
        }
    }
};
 
__KERNEL__ void ShadowKernel(__CONSTANTREF__ RaysParams *params, __TEXTURE2D__ in, __TEXTURE2D__ src, __TEXTURE2D_WRITE__ out) {
    DEFINE_KERNEL_ITERATORS_X(rayNum)
 
    EvaluateShadow(in, src, out, rayNum, to_int2(params->workSize[0], params->workSize[1]), params->blend, to_float2(params->center[0], params->center[1]),
                params->decay, params->weight, params->exposure, params->threshold, params->gamma, params->low, params->high, params->mergeOver,to_float3_v(params->color), params->compOrder);
}
]]
 
--------------------------------
-- Fusion tool functions
 
function Create()
InLabel = self:AddInput("Real Shadow 2.0 | By Learn Now FX", "version", {
	LINKID_DataType	= "Text",
    IC_ControlPage = -1,
	INPID_InputControl = "LabelControl",
	INP_External = false,
	INP_Passive	= true,
})
InMode = self:AddInput("Type", "Type", {
	LINKID_DataType = "Number",
	INPID_InputControl = "ComboControl",
	INP_MinScale  = 0,
	INP_MaxScale = 1,
	INP_MinAllowed = 0,
	INP_MaxAllowed = 1,
	INP_Default = 0,
	INP_Integer = true,
	ICD_Width = 1.0,
	CC_LabelPosition = "Horizontal",
	INP_DoNotifyChanged = true,
	{ CCS_AddString = "Real Shadow", },
	{ CCS_AddString = "Long Shadow", },
})
self:BeginControlNest("Light Settings", "LightSettings", true, {})
InCenter = self:AddInput("Light Position", "LightPosition", {
    LINKID_DataType = "Point",
    INPID_InputControl = "OffsetControl",
    INPID_PreviewControl = "CrosshairControl",
    INP_DefaultX = 0.0,
    INP_DefaultY = 1.0,
})
InDistance = self:AddInput("Light Distance", "LightDistance", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_MinScale = 1.0,
    INP_MaxScale = 10.0,
    INP_Default = 5.0,
})
self:EndControlNest()	 
self:BeginControlNest("Shadow Settings", "Light", true, {})
InBlend = self:AddInput("Blend", "Blend", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_Default = 1,
})
InDecay = self:AddInput("Decay", "Decay", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_MinScale = 0.01,
    INP_MaxScale = 0.2,
    INP_Default = 0.04,
	IC_Visible = false,
})
InWeight = self:AddInput("Weight", "Weight", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_MinScale = 0.0,
    INP_MaxScale = 10.0,
    INP_Default = 0.5,
	IC_Visible = false,
})
InExposure = self:AddInput("Strength", "Strength", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_MinScale = 0.0,
    INP_MaxScale = 20.0,
    INP_Default = 2.0,
	IC_Visible = false,
})
InThreshold = self:AddInput("Threshold", "Threshold", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_MinScale = 0.0,
    INP_MaxScale = 1.0,
    INP_Default = 0.8,
})
InGamma = self:AddInput("Gamma", "Gamma", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_MinScale = 0.0,
    INP_MaxScale = 5.0,
    INP_Default = 1.0,
})
InLow = self:AddInput("Low", "Low", {
	LINKID_DataType = "Number",
    INPID_InputControl = "RangeControl",
    RNGCS_MidName = "Range",
	INP_Default  = 0.0,
	INP_MaxScale = 1.0,
	INP_MinScale = 0.0,
	IC_ControlGroup = 2,
	IC_ControlID = 0,
	IC_Visible = false,
})				
InHigh = self:AddInput("High", "High", {
	LINKID_DataType = "Number",
	INPID_InputControl = "RangeControl",
	INP_Default  = 1.0,
	INP_MaxScale = 1.0,
	INP_MinScale = 0.0,
	IC_ControlGroup = 2,
	IC_ControlID = 1,
	IC_Visible = false,
})
InMergeOver = self:AddInput("Merge Over", "MergeOver", {
    LINKID_DataType = "Number",
    INPID_InputControl = "CheckboxControl",
    INP_Integer = true,
    INP_Default = 1.0,
})
self:EndControlNest()
self:BeginControlNest("Shadow Color", "ShadowColor", true, {})
InColorR = self:AddInput("Red", "Red", {
  ICS_Name            = "Color",
  LINKID_DataType     = "Number",
  INPID_InputControl  = "ColorControl",
  INP_Default         = 0.1,
  INP_MaxScale        = 1.0,
  ICD_Center          = 1.0,
  INP_DoNotifyChanged = true,
  CLRC_ShowWheel      = true,
  IC_ControlGroup     = 1,
  IC_ControlID        = 0,
  IC_Visible          = true,
})
InColorG = self:AddInput("Green", "Green", {
  LINKID_DataType     = "Number",
  INPID_InputControl  = "ColorControl",
  INP_Default         = 0.1,
  INP_DoNotifyChanged = true,
  IC_ControlGroup     = 1,
  IC_ControlID        = 1,
})
InColorB = self:AddInput("Blue", "Blue", {
  LINKID_DataType     = "Number",
  INPID_InputControl  = "ColorControl",
  INP_Default         = 0.1,
  INP_DoNotifyChanged = true,
  IC_ControlGroup     = 1,
  IC_ControlID        = 2,
})
self:EndControlNest()	 
InImage = self:AddInput("Image", "Image", {
  LINKID_DataType = "Image",
  LINK_Main = 1,
})
OutImage = self:AddOutput("Output", "Output", {
  LINKID_DataType = "Image",
  LINK_Main = 1,
})
end
 
 
function Process(req)
    local src = InImage:GetValue(req)
		local inimg = src:ChannelOpOf("Copy", nil, {R = "fg.A", G = "fg.A", B = "fg.A", A = "fg.A"})
		local mode = InMode:GetValue(req).Value
        local distance = InDistance:GetValue(req).Value
		local center = { (((InCenter:GetValue(req).X)- 0.5)*distance)+0.5, (((InCenter:GetValue(req).Y)-0.5)*distance)+0.5 }
		local decay = -InDecay:GetValue(req).Value
		local weight = InWeight:GetValue(req).Value
		local exposure = InExposure:GetValue(req).Value
		local threshold = InThreshold:GetValue(req).Value
		local low = InLow:GetValue(req).Value
		local high = InHigh:GetValue(req).Value
	if mode == 1 then
		decay = -0.01
		weight = 700.0
		exposure = 700.0
		low = 0.0
		high = 0.001
	end		
		
 
    local node = DVIPComputeNode(req, "ShadowKernel", source, "RaysParams", params)
 
    local params = node:GetParamBlock(params)
 
    params.blend = InBlend:GetValue(req).Value
    params.decay = decay
    params.weight = weight
    params.exposure = exposure
    params.threshold = threshold
    params.gamma = InGamma:GetValue(req).Value
    params.low = low
    params.high = high
    params.mergeOver = InMergeOver:GetValue(req).Value
    params.color[0] = InColorR:GetValue(req).Value
    params.color[1] = InColorG:GetValue(req).Value
    params.color[2] = InColorB:GetValue(req).Value
    params.center = center
    params.workSize = { src.DataWindow:Width(), src.DataWindow:Height() }
    params.compOrder = src:IsMask() and 1 or 15
 
    node:SetParamBlock(params)
 
    local numRays = 2 * (src.Width + src.Height - 2) + 1
    node:SetGlobalSize(math.ceil(numRays / 128) * 128)
    node:SetWorkSize(128)
 
    local out = Image{ IMG_Like = src, IMG_DeferAlloc = true }
 
    node:AddInput("in", inimg)
		node:AddInput("src", src)
    node:AddOutput("out", out)
 
    local ok = node:RunSession(req)
 
    if not ok then
        dst = nil
        dump(node:GetErrorLog())
    end
 
    OutImage:Set(req, out)
end
function NotifyChanged(inp, param, time)
	if inp ~= nil and param ~= nil then
		if inp == InMode then
			if param.Value == 1.0 then
				InDecay:SetAttrs({ IC_Visible = false,})
				InWeight:SetAttrs({ IC_Visible = false,})
				InExposure:SetAttrs({ IC_Visible = false,})
				InLow:SetAttrs({ IC_Visible = false,})
				InHigh:SetAttrs({ IC_Visible = false,})
			else
				InDecay:SetAttrs({ IC_Visible = true,})
				InWeight:SetAttrs({ IC_Visible = true,})
				InExposure:SetAttrs({ IC_Visible = true,})
				InLow:SetAttrs({ IC_Visible = true,})
				InHigh:SetAttrs({ IC_Visible = true,})
			end
	  end
  end
end 