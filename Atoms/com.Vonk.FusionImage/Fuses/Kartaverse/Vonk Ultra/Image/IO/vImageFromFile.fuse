
-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vImageFromFile"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Image\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Reads an Image object from a file.",
    REGS_OpIconString = FUSE_NAME,
    
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
})

function Create()
    -- [[ Creates the user interface. ]]
    InFile = self:AddInput("Input" , "Input" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        LINK_Main = 1
    })

    InTimeMode = self:AddInput("Time Mode", "TimeMode", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ComboControl",
        INP_Default = 0,
        INP_Integer = true,
        ICD_Width = 1,
        {CCS_AddString = "Static Frame"},
        {CCS_AddString = "Current Frame"},
        {CCS_AddString = "Request Time"},
        {CCS_AddString = "Time"},
        CC_LabelPosition = "Vertical",
        INP_DoNotifyChanged = true
    })

    InTime = self:AddInput("Time", "Time", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.0,
        INP_MaxScale = 100.0,
        INP_Default = 0.0,
        LINK_Main = 2
    })

--    InViewFile = self:AddInput('View File', 'View File', {
--        LINKID_DataType = 'Number',
--        INPID_InputControl = 'ButtonControl',
--        INP_DoNotifyChanged = true,
--        INP_External = false,
--        ICD_Width = 1,
--        INP_Passive = true,
--        IC_Visible = true,
--        BTNCS_Execute = [[
---- check if a tool is selected
--local selectedNode = tool or comp.ActiveTool
--if selectedNode then
--    local filename = comp:MapPath(selectedNode:GetInput('Input'))
--    if filename then
--        if bmd.fileexists(filename) then
--            bmd.openfileexternal('Open', filename)
--        else
--            print('[View File] File does not exist yet:', filename)
--        end
--    else
--        print('[View File] Filename is nil. Possibly the text is sourced from an external text input connection.')
--    end
--else
--    print('[View File] Please select the node.')
--end
--        ]],
--    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutData = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Image",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
        InTime:SetAttrs({LINK_Visible = visible})
    elseif inp == InTimeMode then
        local visible
        if param.Value == 3.0 then visible = true else visible = false end

        InTime:SetAttrs({IC_Visible = visible})
    end
end


function parse_filename(filename)
    --[[
        parseFilename() from bmd.scriptlib
        A function for ripping a filepath into little bits

        :param FullPath : The raw, original path sent to the function
        :param FullPathMap : The PathMap expanded original path sent to the function
        :param Path : The path, without filename
        :param PathMap : The PathMap expanded path, without filename
        :param FullName : The name of the clip w\ extension
        :param Name : The name without extension
        :param CleanName: The name of the clip, without extension or sequence
        :param SNum : The original sequence string, or "" if no sequence
        :param Number : The sequence as a numeric value, or nil if no sequence
        :param Extension: The raw extension of the clip
        :param Padding : Amount of padding in the sequence, or nil if no sequence
        :param UNC : A true or false value indicating whether the path is a UNC path or not

        :rtype: returns a table with the following
    ]]

    local seq = {}
    seq.FullPath = filename
    seq.FullPathMap = self.Comp:MapPath(filename)
    string.gsub(seq.FullPath, "^(.+[/\\])(.+)", function(path, name) seq.Path = path seq.FullName = name end)
    string.gsub(seq.FullPath, "^(.+[/\\])(.+)", function(path, name) seq.PathMap = self.Comp:MapPath(path) seq.FullName = name end)
    string.gsub(seq.FullName, "^(.+)(%..+)$", function(name, ext) seq.Name = name seq.Extension = ext end)

    if not seq.Name then -- no extension?
        seq.Name = seq.FullName
    end

    string.gsub(seq.Name, "^(.-)(%d+)$", function(name, SNum) seq.CleanName = name seq.SNum = SNum end)

    if seq.SNum then
        seq.Number = tonumber(seq.SNum)
        seq.Padding = string.len(seq.SNum)
    else
         seq.SNum = ""
         seq.CleanName = seq.Name
    end

    if seq.Extension == nil then seq.Extension = "" end
    seq.UNC = (string.sub(seq.Path, 1, 2) == [[\\]])

    return seq
end

function Process(req)
    -- Read the filename
    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)
    local filename = abs_path
    
    local t = InTime:GetValue(req).Value

    -- Link connection port
    -- Todo: Possibly read and merge in the "Link" input connection's image metadata stream with the GetFrame native metadata Lua table values
    --local imgLinkConnection = InLink:GetSource(req.Time, req:GetFlags())

    -- Verify the file exists
    if bmd.fileexists(filename) == true then
        local frame

        local time_mode = InTimeMode:GetValue(req).Value
        if time_mode == 0 then
            -- Static Frame
            local tbl = parse_filename(filename)
            frame = tbl.Number or 0
        elseif time_mode == 1 then
             -- Current Frame
            frame = self.Comp.CurrentTime
        elseif time_mode == 2 then
            -- Request Time
            frame = req.Time
        elseif time_mode == 3 then
            -- Time Input
            frame = tonumber(t)
        end

        if frame == nil then
          frame = 0
        end

        -- Get the image clip
        -- local clip = Clip(filename)

        -- Fusion 16.1+
        local clip = Clip(filename, false)

        -- Read the first frame of the image sequence
        -- out = clip:GetFrame(frame)

        -- Fusion 16.1+
        clip:Open()
        out = clip:GetFrame(frame)
        clip:Close()

        -- print('[Image] [Filename]', clip.Filename, '[Frame]', frame)

        -- Push the result to the node's output connection
        OutData:Set(req, out)
    else
        -- Fallback to a blank canvas when no image is found
        local compWidth = self.Comp:GetPrefs('Comp.FrameFormat.Width') or 1920
        local compHeight = self.Comp:GetPrefs('Comp.FrameFormat.Height') or 1080

        emptyImage = Image({
            IMG_Width = compWidth,
            IMG_Height = compHeight,
        })

        -- Pixel defaults to black/clear
        emptyImage:Fill(Pixel())

        -- Push the empty result to the node's output connection
        OutData:Set(req, emptyImage)
    end
end
