# Vonk Ultra - Vector

Vector is a node based vector library for Blackmagic Design Fusion.

## Acknowledgements

- Kristof Indeherberge
- Cédric Duriau

## Requirements

- Vonk Matrix

## Contents

**Fuses**

- `vVectorAdd.fuse`: Fuse to add two vectors.
- `vVectorDivideNumber.fuse`: Fuse to divide a vector by a number.
- `vVectorFromArray.fuse`: Fuse to create a vector from an array stored as JSON string.
- `vVectorLength.fuse`: Fuse to calculate the length of a vector.
- `vVectorMultiplyNumber.fuse`: Fuse to multiply a vector by a number.
- `vVectorSubtract.fuse`: Fuse to subtract two vectors.
