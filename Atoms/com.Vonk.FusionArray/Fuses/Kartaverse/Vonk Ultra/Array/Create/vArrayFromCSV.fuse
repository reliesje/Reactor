-- ============================================================================
-- modules
-- ============================================================================
local filesystemutils = self and require("vtextutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vArrayFromCSV"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Array\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a JSON array from a CSV row or column",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
})

function Create()
    -- [[ Creates the user interface. ]]
    InMode = self:AddInput("Array Mode", "Mode", {
        LINKID_DataType = "Number",
        INPID_InputControl = "MultiButtonControl",
        MBTNC_ForceButtons = true,
        --MBTNC_ShowBasicButton = false,
        MBTNC_ShowName = true,
        INP_External = false,
        INP_Default = 0,
        {MBTNC_AddButton = "Extract Row" , MBTNCD_ButtonWidth = 0.5},
        {MBTNC_AddButton = "Extract Column" , MBTNCD_ButtonWidth = 0.5},
    })
    InIndex= self:AddInput("Index", "index", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 144,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MaxAllowed = 1000000,
        INP_Integer = true,
        LINK_Main = 2
    })
    InIgnoreHeaderRow = self:AddInput("Ignore Header Row", "IgnoreHeaderRow", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })
    InData = self:AddInput("Input", "Input", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 10,
        LINK_Main = 1
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 10,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_Passive = true,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutData = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InIndex:SetAttrs({LINK_Visible = visible})
        -- InData:SetAttrs({LINK_Visible = visible})
    elseif inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InData:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InData:SetAttrs({IC_Visible = false})
        InData:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InData:SetAttrs({TEC_Lines = lines})
    
        -- Toggle the visibility to refresh the inspector view
        InData:SetAttrs({IC_Visible = false})
        InData:SetAttrs({IC_Visible = true})
    end
end


function Process(req)
    -- [[ Creates the output. ]]
    local data = InData:GetValue(req).Value

    local mode = InMode:GetValue(req).Value

    local header = tonumber(InIgnoreHeaderRow:GetValue(req).Value)
    local index = tonumber(InIndex:GetValue(req).Value)

    local pattern = "(.-),"

    local result

    if mode == 0 then
        -- Row

        -- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
        local line = tostring(textutils.ReadLine(data, index + header)) .. ","

        local replaced = textutils.split(line, pattern)
        local array = textutils.array_from_string(replaced)

        -- print("[Data]\n", data)
        -- print("[Index]", index)
        -- print("[Ignore Header Row]", header)
        -- print("[Line]", line)
        -- print("[Split]")
        -- dump(replaced)
        -- print("[Array]\n", array)

        result = array
    else
        -- Column
        local column = {}
        currentLine = 0

        for l in string.gmatch(data, "[^\r\n]+") do
            currentLine = currentLine + 1; 
            -- print(currentLine, ":", l)

            if currentLine == 1 and header == 1 then
                -- print("[Skipping Header Row]")
            else
                -- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
                local line = l .. ","

                local replaced = textutils.split(line, pattern)
                table.insert(column, replaced[index]) 
            end
        end

        -- print("[Column Lua Table")
        -- dump(column)

        result = textutils.array_from_string(column)
    end
    
    local out = Text(result)

    OutData:Set(req, out)
end
