--[[--
Restart Fusion - v2.2 2022-08-27
By Andrew Hazelden <andrew@andrewhazelden.com>

## Overview ##

The "Restart Fusion" menu item makes it easy to quit and re-open Fusion. This is handy if you are creating your own fuses, plugins, or .fu files and have to reload Fusion each time you make an edit to see the results of your latest changes. The foreground comp is opened up automatically when Fusion is re-launched.

The "Restart Fusion.fu" config file adds a "File > Restart Fusion" menu item. You can also press the Alt+Q (Windows/Linux) or Option+Q (Mac) hotkeys to restart Fusion.

This script requires Fusion 9.0.2-18+ or Resolve 15-18+ to run.

## Installation ##

Step 1. Move the "Restart Fusion.fu" file into the Fusion user prefs "Config:/" folder.

Step 2. Restart Fusion once so the config file is loaded.
--]]--

{
	Action
	{
		ID = "Restart_Fusion",
		Category = "File",
		Name = "Restart Fusion",

		Targets =
		{
			Composition =
			{
				Execute = _Lua [=[
-- Find out the current Fusion host platform (Windows/Mac/Linux)
platform = (FuPLATFORM_WINDOWS and "Windows") or (FuPLATFORM_MAC and "Mac") or (FuPLATFORM_LINUX and "Linux")

-- Get the Fusion program path:
-- /Applications/Blackmagic Fusion 9/Fusion.app/Contents/MacOS/Fusion
fusionApp = fusion:GetAttrs().FUSIONS_FileName

-- Get the active comp filename
sourceComp = app:MapPath(obj:Comp():GetAttrs().COMPS_FileName) or ""

-- Create the Fusion launching command
if platform == "Windows" then
	command = "start \"\" " .. "\"" .. fusionApp .. "\" \"" .. sourceComp .. "\" 2>&1 &"
elseif platform == "Mac" then
	command = "\"" .. fusionApp .. "\" \"" .. sourceComp .. "\"  2>&1 &"
	-- command = "open \"" .. fusionApp .. "\" \"" .. sourceComp .. "\" 2>&1 &"
else
	command = "\"" .. fusionApp .. "\" \"" ..  sourceComp .. "\" 2>&1 &"
end

obj:Comp():Print("[Launch Command] " .. tostring(command) .. "\n")

-- Start up a new instance of Fusion
os.execute(command)

-- Quit the current Fusion session
fusion:Quit()
				]=],
			},
		},
	},

	Hotkeys
	{
		Target = "FlowView",
		
		ALT_Q = "Restart_Fusion{}",
	},

	Hotkeys
	{
		Target = "GLView",
		
		ALT_Q = "Restart_Fusion{}",
	},

	Hotkeys
	{
		Target = "FuFrame",
		
		ALT_Q = "Restart_Fusion{}",
	},

	Hotkeys
	{
		Target = "ConsoleView",
		
		ALT_Q = "Restart_Fusion{}",
	},

	Menus
	{
		Target = "ChildFrame",

		After "File\\Background Render"
		{
			"-",
			"Restart_Fusion{}",
		},
	},

}
