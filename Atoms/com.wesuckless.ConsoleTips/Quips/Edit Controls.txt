{[[The Edit Controls feature can not only be used to add new inputs, it can also change the properties of existing inputs—expand their ranges, move them to different tabs, or even change their control widget!

-- Bryan Ray]]}
