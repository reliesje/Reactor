{[[Placing a "--" in a simple expression will ignore the characters to the right. As in "3.14159 -- pi to 5 decimals"

-- Chad Capeland]]}
