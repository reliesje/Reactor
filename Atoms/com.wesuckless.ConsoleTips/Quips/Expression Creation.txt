{[[An easy way to add a new expression to a number field in Fusion is to type in an = character first before the rest of the expression string is entered. This means if you want the final expression to be width*0.5 you would type in =width*0.5.

-- Andrew Hazelden]]}
