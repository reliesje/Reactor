{[[Here's a single-line command you can paste into the Console to get the name and render time of all selected tools:

for i,tool in ipairs(comp:GetToolList(true)) do
	print(tool:GetAttrs().TOOLS_Name..": "..tool:GetAttrs().TOOLN_LastFrameTime);
end

-- Bryan Ray]]}
