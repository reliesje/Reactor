{[[You can read the image metadata on a Loader node using the Lua command:

meta = Loader1.Output[comp.CurrentTime].Metadata;
dump(meta).

Then the individual metadata items can be accessed by their the Lua table element name like this:

dump(meta["Filename"])

-- Andrew Hazelden]]}
