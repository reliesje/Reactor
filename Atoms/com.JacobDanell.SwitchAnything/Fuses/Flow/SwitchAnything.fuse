--[[--
SwitchAnything.Fuse

Switch between multiple inputs of any kind.

----------------------------------------------------------------------
Copyright (c) 2022-02-03, Jacob Danell <jacob@emberlight.se>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
----------------------------------------------------------------------

Changelog:
v1.0.1, 2010-02-24:
- Fixed faulty outputs
- Had to remove Particle type as it didn't work

v1.0, 2022-02-03:
- Initial release

--]]--


FuRegisterClass("SwitchAnything", CT_Tool, {
	REGS_Name = "Switch Anything",
	REGS_Category = "Flow",
	REGS_OpIconString = "SwA",
	REGS_OpDescription = "Switch between multiple inputs of any kind",
	
	REGS_Company = "Jacob Danell",
	REGS_URL = "http://www.emberlight.se",
	REG_Version = 101,

	REG_NoPreCalcProcess = true,	-- let Process() handle the PreCalcProcess() stuff as well
	REG_OpNoMask = true,
	REG_NoBlendCtrls = true,
	REG_NoObjMatCtrls = true,
	REG_NoMotionBlurCtrls = true,
	REG_SupportsDoD = true,
	REG_Fuse_NoEdit = false,
	REG_Fuse_NoReload  = false,
})
	
function Create()
	self:RemoveControlPage('Controls')
	self:AddControlPage('Switch')

	InMode = self:AddInput('Data Type', 'DataType', {
		{ CCS_AddString = '--Select Mode--', CCID_AddID = 'SelectMode', },
		{ CCS_AddString = 'Image', CCID_AddID = 'Image', },
		{ CCS_AddString = 'Mask', CCID_AddID = 'Mask', },
		{ CCS_AddString = '3D', CCID_AddID = 'DataType3D', },
		{ CCS_AddString = 'Material', CCID_AddID = 'MtlGraph3D', },
		LINKID_DataType = 'FuID',
		LINKS_Name = 'Data Type',
		ICS_ControlPage = 'Switch',
		INPID_InputControl = 'ComboIDControl',
		IC_Visible = true,
		INP_External = false,
		INP_DoNotifyChanged = true,
		INP_InitialNotify   = true,
		INPID_DefaultID = 'SelectMode',
		INPI_Priority = 1,
		ICD_Width = 1.0,
		})

	InWhich = self:AddInput("Which", "Which", {
		LINKID_DataType    = "Number",
		INPID_InputControl = "SliderControl",
		INP_Default 	   = 0.0,
		INP_MinAllowed     = 1,
		INP_MaxAllowed     = 64,
		INP_MaxScale       = 1,
		INP_Integer        = true,
		IC_Steps           = 1.0,
		IC_Visible 		   = false,
		})

end

------------------------- DYNAMIC GUI -------------------------
function NotifyChanged(inp, param, time)
	if inp ~= nil and param ~= nil then
		if inp == InMode then
			if param.Value == 'SelectMode' then
--				Do Nothing
			else
				InMode:SetAttrs({ IC_Visible = false, })
				InWhich:SetAttrs({ IC_Visible = true, })
				In = self:AddInput('Input1', 'Input1', {
					LINKID_DataType = param.Value,
					INPID_InputControl = 'ImageControl',
					ICS_ControlPage = 'Switch',
					LINK_Main = 1,
					IC_Visible = false,
					INP_SendRequest = false, 	-- don't send a request for this branch before we actually need it.
					INP_Required = false,
					})
				Out = self:AddOutput('Output', 'Output', {
					LINKID_DataType = param.Value,
					LINK_Main = 1,
					LINK_Visible = true,
					})
			end
		end
	end
end
---------------------------------------------------------------


function OnAddToFlow()
	-- If a comp is reopened, we need to recreate all inputs that might have
	-- been saved. The weird thing is, that FindInput tells us that an input
	-- exists while it's not visible in the GUI. So we just call AddInput
	-- again, which will make the triangle show up in the GUI.
	-- A problem arises if, for example, inputs 1 and 3 are connected, but 2
	-- isn't. Since Input2 won't be saved in the comp we first need to look
	-- for the highest input we need. Afterwards, OnConnected() will be called
	-- for each of the saved inputs. The additional input needed to connect
	-- further images will be created there.
	local highestInput = 0
	for i = 1, 64 do
		if self:FindInput("Input"..tostring(i)) ~= nil then
			highestInput = i
		end
	end

	if highestInput > 0 then
		local dataType = InMode:GetSource(0).Value
		for i = 2, highestInput do
			self:AddInput("Input"..i, "Input"..i, {
				LINKID_DataType = dataType,
				INPID_InputControl = 'ImageControl',
				ICS_ControlPage = 'Switch',
				LINK_Main = i,
				IC_Visible = false,
				INP_SendRequest = false,
				INP_Required = false,
				})
		end
	end

	InWhich:SetAttrs({INP_MaxScale = highestInput, INP_MaxAllowed = highestInput})
end


-- OnConnected gets called whenever a connection is made to the inputs. A new
-- input is created if something has been connected to the highest input.
function OnConnected(inp, old, new)
	local inpNr = tonumber(string.match(inp:GetAttr("LINKS_Name"), "Input(%d+)"))
	local dataType = InMode:GetSource(0).Value
	local maxNr = tonumber(InWhich:GetAttr("INP_MaxAllowed"))
	if inpNr then
		if inpNr >= maxNr and maxNr < 64 and new ~= nil then
			InWhich:SetAttrs({INP_MaxScale = inpNr, INP_MaxAllowed = inpNr})
			self:AddInput("Input"..(inpNr + 1), "Input"..(inpNr + 1), {
				LINKID_DataType = dataType,
				INPID_InputControl = 'ImageControl',
				ICS_ControlPage = 'Switch',
				LINK_Main = (inpNr + 1),
				IC_Visible = false,
				INP_SendRequest = false,
				INP_Required = false,
				})
		end
	end
end


function Process(req)
	local which = InWhich:GetValue(req).Value
	inp = self:FindInput("Input"..which)
	local img = nil

	if inp ~= nil then
		local img = inp:GetSource(req.Time, req:GetFlags())
		if img ~= nil then
			Out:Set(req, img)
		end
	end
end