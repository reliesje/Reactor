--[[

Sprut2 for Fusion

-------------------------------------------------------------------
Copyright (c) 2020,  Pieter Van Houte
<pieter[at]secondman[dot]com>
-------------------------------------------------------------------

Sprut (c) 2012-2014 Theodor Groeneboom - www.euqahuba.com (theo@euqahuba.com)

Sprut2 is distributed under GNU/GPL. It may be used for commercial purposes, but not redistributed or repackaged, in particular not for any fee.

--]]

require "SprutDialogs"

local comp = fusion.CurrentComp

if comp:IsRendering() == true then
	comp:SetData ("Sprut Render Abort", true)

	SprutMsgDialog("Sprut", "Render Aborting...", "OK")
else
	SprutMsgDialog("Sprut", "Nothing appears to be rendering right now...", "OK")
end